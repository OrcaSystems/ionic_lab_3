import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewsPage} from '../news/news'
import { NewsApiProvider } from '../../providers/news-api/news-api';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public listSources=[];
  constructor(public navCtrl: NavController, public newsApi:NewsApiProvider) {
    this.listSources=[];
    newsApi.getSources().subscribe(response=>{
      this.listSources=response['sources'];
      //console.log(this.listSources);
    });
  }
  public goToPageNews(id_source){
    this.navCtrl.setRoot(NewsPage,{id:id_source});
  }

}
